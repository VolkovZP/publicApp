import * as yup from 'yup';

export const EMAIL_SCHEMA = yup
  .string()
  .email()
  .required();

export const LOGIN_SCHEMA = yup
  .string()
  .matches(
    /^(?=.*[A-Za-z0-9]$)[A-Za-z][A-Za-z\d.-]{0,19}$/,
    'length must be no more  19 characters'
  )
  .required();

export const PASSWORD_SCHEMA = yup
  .string()
  .max(20)
  .min(3)
  .required();

export const REPEAT_PASSWORD_SCHEMA = yup
  .string()
  .oneOf([yup.ref('password'), null], 'Passwords must match')
  .required()


export const REGISTER = yup.object({
  email: EMAIL_SCHEMA,
  password: PASSWORD_SCHEMA,
  repeatPassword: REPEAT_PASSWORD_SCHEMA,
  login: LOGIN_SCHEMA,
});

export const LOGIN = yup.object({
  password: PASSWORD_SCHEMA,
  email: EMAIL_SCHEMA,
});