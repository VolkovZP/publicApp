import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { ApolloProvider } from '@apollo/client';
import client from './GraphQL/client';

test('renders learn react link', () => {
  render(
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  );
});
