import { gql } from '@apollo/client';

export const registerMutation = gql`
  mutation register(
    $avatar: String!
    $password: String!
    $email: String!
    $login: String!
  ) {
    registration(
      avatar: $avatar
      password: $password
      email: $email
      login: $login
    ) {
      user {
        email
        avatar
        login
        id
      }
      token
    }
  }
`;
