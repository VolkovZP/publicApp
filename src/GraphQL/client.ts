import { ApolloClient, InMemoryCache ,ApolloLink,HttpLink,concat } from '@apollo/client';
import { CONSTANS } from '../CONSTANTS';


const httpLink = new HttpLink({ uri: CONSTANS.BASE_URL});


const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      ["access-token"]: localStorage.getItem('token') || null,
    }
  }));

  return forward(operation);
})


const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink),
});

export default client;




