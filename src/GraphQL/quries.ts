import { gql } from "@apollo/client";

export const loginQuries = gql`
      query login($password: String!, $email: String!,) {
        signIn(password: $password, email:$email) {
          user {
              email,
              avatar
              login
              id
          }
          token
        }
      }
    `;



export const onlineUserQurie = gql`
query{
  me{
    user{
      id
      email
      login
      avatar
    }
    token
  }
}
`;

export const getAllMessages = gql`
query{
  getAllMessages{
    id
    description
    userId
    convId
    date
    user{
      avatar
      id
		login
    }
  }
}
`;