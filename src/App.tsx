import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.sass'
import Login from './pages/Login';
import Registration from './pages/Registration';
import Layout from './components/Layout';
import WrapperContext from './components/WrapperContext';
import Header from './components/Header';

function App() {
  return (
    <WrapperContext>
      <Router>
        <Header headerTitle='GQL Chat' />
        <Layout>
          <Routes>
            <Route path='/register' element={<Registration />} />
            <Route path='/login' element={<Login />} />
          </Routes>
        </Layout>
      </Router>
    </WrapperContext>
  )
}

export default App;
