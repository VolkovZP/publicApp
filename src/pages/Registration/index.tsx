import React, { useContext, useState } from 'react';
import style from './Registration.module.sass';
import { Formik, Form, FormikHelpers } from 'formik';
import Button from '../../components/Button';
import { REGISTER } from '../../utils';
import TitleForm from '../../components/TitleForm';
import { userContext } from '../../contexts';
import FormikInput from '../../components/FormikInput';
import AvatarContainer from '../../components/AvatarInput';
import FormErrorMessage from '../../components/FormErrorMessage';

export interface IRegisterValues {
  [key: string]: string;
}

const initialValues: IRegisterValues = {
  login: '',
  email: '',
  password: '',
  repeatPassword: '',
  avatar: '',
};


export default function Registration() {
  const { user, register } = useContext(userContext)
  const [registerError, setRegisterError] = useState<any>('')
  const onSubmit = async (
    values: IRegisterValues,
    { setSubmitting, resetForm }: FormikHelpers<IRegisterValues>
  ) => {
    try {
      await register({ variables: values })
      resetForm();
      setRegisterError('')
      setSubmitting(false);
    } catch (err) {
      setRegisterError(err)
      throw err
    }
    return false
  };

  return (
    <div>
      <TitleForm>Registration</TitleForm>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={REGISTER}
      >
        {({ values: { avatar: linkAvatar } }) => {
          return (
            <Form className={style['registration-form']}>
              <FormikInput name='login' label='Login' msgError={registerError} />
              <FormikInput name='email' label='Email' msgError={registerError} />
              <FormikInput name='password' label='Password' type='password' msgError={registerError} />
              <FormikInput name='repeatPassword' label='Repeat password' type='password' msgError={registerError} />
              <AvatarContainer name='avatar' label='url' logo='Logo' link={linkAvatar} />
              <div>
                <Button type='submit'>Register</Button>
              </div>
              <FormErrorMessage error={registerError} />
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}
