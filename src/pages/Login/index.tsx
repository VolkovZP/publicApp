import React, { useContext, useState } from 'react';
import style from './Login.module.sass';
import { NavLink } from 'react-router-dom';
import { Formik, Form, FormikHelpers } from 'formik';
import Button from '../../components/Button';
import { LOGIN } from '../../utils';
import TitleForm from '../../components/TitleForm';
import { userContext } from '../../contexts';
import FormErrorMessage from '../../components/FormErrorMessage';
import FormikInput from '../../components/FormikInput';



export interface ILoginValues {
  [key: string]: string;
}

const initialValues: ILoginValues = {
  password: '',
  email: '',
};


export default function Login() {
  const { user, login } = useContext(userContext);
  const [loginError, seLoginError] = useState<any>('')

  const onSubmit = async (values: ILoginValues, { setSubmitting, resetForm }: FormikHelpers<ILoginValues>
  ) => {
    try {
      await login(values);
      setSubmitting(true);
      resetForm();
      seLoginError('')
    } catch (err) {
      seLoginError(err)
      throw err
    }
    return false;
  };


  return (
    <div className={style.container}>
      <TitleForm>Welcome</TitleForm>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={LOGIN}
      >
        {() => {
          return (
            <Form>
              <FormikInput name='email' label='Email' msgError={loginError} />
              <FormikInput name='password' label='Password' type='password' msgError={loginError} />
              <div className={style['btn-container']}>
                <NavLink className={style.link} to='/register'>
                  Registration
                </NavLink>
                <Button size='small' type='submit'>Login</Button>
              </div>
              <FormErrorMessage error={loginError} />
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}
