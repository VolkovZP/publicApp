import React from 'react';
import style from './Input.module.sass';
import cx from 'classnames';


interface IinputComponent {
  placeholder?: string;
  type?: "text" | "password" | "email"
  errorMessage?: string;
  label?: string;
  value?: string;
  name?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}



function Input({
  placeholder,
  errorMessage,
  label,
  value,
  name,
  type = 'text',
  onChange,
}: IinputComponent) {



  const valid = cx(style.input, { [style['error-input']]: errorMessage }, style[`size-${type}-input`]);
  return (

    <>
      <label className={style.label}>{label}</label>
      <input
        name={name}
        className={valid}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        type={type}
      />
      {errorMessage && (
        <span className={style['error-text']}>{typeof errorMessage === 'string' ? errorMessage : ''}</span>
      )}
    </>
  );
}
export default Input;
