import React, { ReactElement, useEffect, useMemo, useRef, useState } from 'react'
import { createPortal } from 'react-dom'
import style from './DropMenu.module.sass'

interface IDropMenu {
    items: ReactElement<any>[];
    target: ReactElement<any>;
    wrapperStyle?: Object;
}

const portal = document.getElementById('portal') as HTMLElement

export const DropMenu = ({ items, target, wrapperStyle }: IDropMenu) => {
    const [isOpen, setOpen] = useState<boolean>()
    const targetRef = useRef<HTMLElement>()

    const toogleDropMenu = () => {
        setOpen(!isOpen)
    }

    const coordinates = useMemo(() => {
        if (targetRef.current) {
            let children = targetRef.current.children[0];
            const coordinates = children.getBoundingClientRect()

            return {
                left: (coordinates?.x - coordinates?.width),
                top: (coordinates?.y + coordinates?.height + 4),
            }
        }
    }, [isOpen]);

    useEffect(() => {
        const handleClick = () => {
            if (isOpen) toogleDropMenu();
        }
        window.addEventListener('click', handleClick, { once: true });
        return () => {
            window.removeEventListener('click', handleClick, true);
        };
    }, [isOpen]);


    return (
        <>
            {isOpen && createPortal(
                <div className={style['menu-container']} style={{
                    position: 'absolute',
                    ...coordinates
                }}>
                    <div className={style['menu-wrapper']}>
                        <nav className={style['nav-menu']} >
                            <ul className={style['list']}>
                                {items.map((item, index) => (<li className={style['list-item']} key={index.toString()}>{item}</li>))}
                            </ul>
                        </nav>
                    </div>
                </div >
                , portal)}
            <span ref={targetRef as React.LegacyRef<HTMLDivElement>} onClick={toogleDropMenu} style={wrapperStyle}> {target}</span>
        </>)
}
