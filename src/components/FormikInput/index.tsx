import React from 'react'
import { Field } from 'formik'
import Input from '../Input'
import style from './FormikInput.module.sass'

interface IFormikInput {
    name: string,
    logo?: string,
    placeholder?: string,
    label?: string,
    type?: 'text' | 'password' | 'email',
    msgError?: string
}



export default function FormikInput({ placeholder, name, label, type, msgError }: IFormikInput) {
    return (
        <Field name={name}>
            {({
                field,
                meta,
            }: any) => (
                <div className={style.container}>
                    <Input errorMessage={meta.touched && meta.error ? meta.error : msgError} placeholder={placeholder} label={label} type={type} {...field} />
                </div>
            )
            }
        </Field >
    )
}
