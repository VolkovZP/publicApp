
import React from 'react'
import style from './FormErrorMessage.module.sass'


function FormErrorMessage({ error }: any) {
    if (!error) return <div>{''}</div>
    return (
        <div className={style.container} >
            {Array.isArray(error) ? error.map((error: any, index: number) =>
                <div className={style['title-error-message']}
                    key={index.toLocaleString()}>{error.message}</div>)
                :
                <div className={style['title-error-message']} >something wrong</div>}
        </div >
    )
}
export default React.memo(FormErrorMessage)