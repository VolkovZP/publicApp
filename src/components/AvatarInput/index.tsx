import React from 'react'
import Avatar from '../Avatar'
import FormikInput from '../FormikInput'
import style from './AvatarInput.module.sass'

interface IAvatarInput {
    name: string,
    logo?: string,
    placeholder?: string,
    label?: string,
    type?: 'text' | 'password' | 'email',
    link?: string
}


export default function AvatarInput({ name, logo, placeholder, label, type = 'text', link }: IAvatarInput) {
    return (
        <div className={style['avatar-container']}>
            <label className={style.label}>{logo}</label>
            <div className={style['avatar-wrapper']}>
                <Avatar size='large' name='FL' link={link} />
                <FormikInput name={name} placeholder={placeholder} label={label} type={type} />
            </div>
        </div>
    )
}
