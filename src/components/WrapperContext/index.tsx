import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react'
import { userContext } from '../../contexts';
import { registerMutation } from '../../GraphQL/mutatuin';
import { loginQuries, onlineUserQurie } from '../../GraphQL/quries';

type typeWrapperContext = {
    children: React.ReactNode;
};



export interface Iuser {
    id: string,
    avatar: string,
    email: string,
    login: string,
}

interface IonlineUserQurie {
    data: {},
    me: {
        user: Iuser
    }
}



export default function WrapperContext({ children }: typeWrapperContext) {
    const client = useApolloClient();
    const [user, setUser] = useState<null | Iuser>(null)


    const { data, loading, error } = useQuery<IonlineUserQurie>(onlineUserQurie)




    useEffect(() => {
        if (data?.me?.user) {
            setUser(data.me.user)
        }
    }, [data])




    const [registerForm] = useMutation(registerMutation, {
        notifyOnNetworkStatusChange: true,
        fetchPolicy: "network-only",
        onCompleted: data => {
            const { user, token } = data.registration
            localStorage.setItem('token', token);
            setUser(user)
            return data
        },
        onError(error) {
            throw error.graphQLErrors
        }
    });



    const login = async (values: any) => {
        try {
            const { data } = await client.query({
                query: loginQuries,
                fetchPolicy: "network-only",
                variables: values
            })
            const { user, token } = data.signIn
            localStorage.setItem('token', token);
            setUser(user)
        } catch (err) {
            if ((err as any).graphQLErrors) {
                throw (err as any).graphQLErrors
            }
            throw err
        }
    }


    if (loading) return null
    return (
        <userContext.Provider value={{
            user,
            setUser,
            login,
            register: registerForm,
        }}>
            {children}
        </userContext.Provider >
    )
}



