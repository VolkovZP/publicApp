import React, { useContext } from 'react';
import Avatar from '../Avatar';
import style from './Message.module.sass'
import cx from 'classnames'
import { userContext } from '../../contexts';
import { formatISO9075, intlFormat, isToday } from 'date-fns';
import { Iuser } from '../WrapperContext';


interface IMessage {
    createdByUser: Iuser
    description: string,
    userId: number | undefined
    date: string | Date
}

export default function Message({ createdByUser, description, userId, date }: IMessage) {

    const { user } = useContext(userContext)

    const getMessageDate = intlFormat(new Date(date), {}, { locale: 'ru-Ru' })
    const getMessageTime = formatISO9075(new Date(date), { representation: 'time' }).slice(0, -3) //slice(0, -3) обрезает секунды, что бы получить часы и минуты.
    const currentDay = isToday(new Date(date))
    const dateCreatedMessage = () => !currentDay ? `${getMessageDate} ${getMessageTime}` : getMessageTime

    return (
        <div className={style.container}>
            <div className={style.wrapper}>
                <div data-testid='row-1' className={cx(style.row, { [style['row-reverse']]: userId == user?.id })}>
                    <Avatar name='FL' link={createdByUser?.avatar} />
                    <div className={style['container-msg']}>
                        <div className={style.msg}>{description}</div>
                        <div className={style['msg-date']}>{dateCreatedMessage()}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
