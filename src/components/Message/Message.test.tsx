import { screen, render } from "@testing-library/react";
import { unmountComponentAtNode } from "react-dom";
import Message from ".";
import { userContext } from "../../contexts";


const fakeUser = { id: '3', avatar: 'https:/sdasdad', email: 'sdfrq@mail.de', login: 'rd2d' }

const fakeMessage = { createdByUser: fakeUser, userId: 3, description: "hello world", date: new Date() };
const fakeMessageNotEquilId = { createdByUser: fakeUser, userId: 4, description: "hello world", date: new Date() };


const providerValue = {
    login: () => { },
    register: () => { },
    setUser: () => { },
    user: fakeUser,
}



let container: any = null;
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);

});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

describe('Message component', () => {
    it('Message render', () => {
        render(<Message {...fakeMessage} />)
        expect(screen.getByText('hello world')).toBeInTheDocument()
    })
    it('if there is no class "row-reverse", the message is mine', () => {
        render(
            <userContext.Provider value={providerValue}>
                <Message {...fakeMessage} />
            </userContext.Provider>
        )
        expect(screen.getByTestId('row-1')).toBeInTheDocument();
        expect(screen.getByTestId('row-1')).toHaveClass('row-reverse')
    }),
        it('if there is no class "row-reverse", the message is not mine', () => {
            render(
                <userContext.Provider value={providerValue}>
                    <Message {...fakeMessageNotEquilId} />
                </userContext.Provider>
            )
            expect(screen.getByTestId('row-1')).toBeInTheDocument();
            expect(screen.getByTestId('row-1')).not.toHaveClass('row-reverse')
        })
})

