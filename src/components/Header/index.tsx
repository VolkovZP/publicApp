import React, { useContext, useEffect, useMemo } from 'react'
import { NavLink } from 'react-router-dom'
import { userContext } from '../../contexts'
import Avatar from '../Avatar'
import style from './Header.module.sass'
import { DropMenu } from '../DropMenu'


interface IHeader {
    headerTitle: string,
}

export default function Header({ headerTitle }: IHeader) {
    const { user, setUser } = useContext(userContext)

    const logout = () => {
        setUser(null)
        localStorage.removeItem('token')
    }



    return (
        <header className={style.container}>
            <div className={style.wrapper}>
                <div className={style.row}>
                    {<NavLink to='/' className={style.logo}>{headerTitle}</NavLink>}
                    {<div className={style['login-link-container']}>{user
                        ?
                        <div>
                            <DropMenu items={
                                [<NavLink to='/profile'>Profile</NavLink>,
                                <div onClick={logout}>Logout</div>]}
                                target={< Avatar link={user.avatar} />} />
                        </div>
                        :
                        <NavLink to='login'>Login</NavLink>}
                    </div>}
                </div>
            </div>
        </header>
    )
}

