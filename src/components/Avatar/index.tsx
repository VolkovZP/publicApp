import React, { useMemo, FunctionComponent } from 'react';
import style from './Avatar.module.sass';
import cx from 'classnames';
import { BASIC_COLOR } from '../../CONSTANTS';

interface IAvatar {
  size?: 'normal' | 'large';
  name?: string;
  link?: string;
}

function getRandomColor(
  min: number = 0,
  max: number = BASIC_COLOR.length - 1
): string {
  min = Math.ceil(min);
  max = Math.floor(max);
  return BASIC_COLOR[Math.floor(Math.random() * (max - min + 1)) + min];
}

const Avatar: FunctionComponent<IAvatar> = (({ size = 'normal', name, link, }: IAvatar) => {




  const memoizedRandomColor = useMemo(() => {
    return getRandomColor();
  }, []);

  const avatarStyle = cx(style.conteiner, memoizedRandomColor, {
    [style['image-normal-size']]: size !== 'large',
  });


  return (
    <div>
      <div className={avatarStyle} >
        {link ? (
          <img className={style.img} src={link} />
        ) : (
          <span className={style.initials}>{name || '??'}</span>
        )}
      </div>
    </div>
  );
})
export default Avatar
