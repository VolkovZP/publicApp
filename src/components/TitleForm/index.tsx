import React from 'react';
import style from './TitleForm.module.sass';
type typeTitleForm = { children: React.ReactNode };

export default function TitleForm ({ children }: typeTitleForm) {
  return <h1 className={style['form-title']}>{children}</h1>;
}
