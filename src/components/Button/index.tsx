import React from 'react';
import style from './Button.module.sass';
import cx from 'classnames';

type typeBtn = {
  children: React.ReactNode;
  type?: 'submit' | 'reset' | 'button' | undefined;
  color?: 'primary' | 'scondary';
  size?: 'small' | 'normal'
};

export default function Button({
  color = 'primary',
  type,
  children,
  size = 'normal',
}: typeBtn) {
  const btnColorStyle = cx(style.btn, `bg-${color}`, {
    [style['small-size-btn']]: size === 'small',
  });

  return (
    <button type={type} className={btnColorStyle}>
      {children}
    </button>
  );
}
