import React from 'react';
import style from './Layout.module.sass';

type typeChatLayout = {
  children: React.ReactNode;
};

export default function Layout ({ children }: typeChatLayout) {
  return (
    <div className={style['out-wrapper']}>
      <div className={style['inner-wrapper']}>{children}</div>
    </div>
  );
}
