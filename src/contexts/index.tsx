import { ApolloCache, DefaultContext, MutationFunctionOptions, OperationVariables } from "@apollo/client";
import { createContext, Dispatch, SetStateAction } from "react";
import { Iuser } from "../components/WrapperContext";

interface IUserContext {
    user: Iuser | null
    setUser: Dispatch<SetStateAction<null | Iuser>>,
    register: (options?: MutationFunctionOptions<any, OperationVariables, DefaultContext, ApolloCache<any>> | undefined) => void
    login: (values: { [key: string]: string }) => Promise<any> | void,
}

export const userContext = createContext<IUserContext>({
    user: null,
    setUser: () => { },
    login: () => { },
    register: () => { },
})

